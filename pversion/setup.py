# Time-stamp: <2020-02-04 09:31:16 daniel>
# -*- mode: python; -*-

# Use Example:
#
#    import pversion
#
#    if pversion.thee : print( 'Running Python3' )
#    else : print( 'Running obsolete Python2' )
#

## Required module
from setuptools import setup

## The main setup function - Called by 'pip install'
setup( name = 'pversion',
       version = '0.1',
       description = 'Version of Python',
       url = '',
       author = 'Archadious',
       author_email = '',
       license = 'MIT',
       packages = [ 'pversion' ],
       zip_safe = True
)  # end setup
