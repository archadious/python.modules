# Time-stamp: <2020-02-04 09:33:34 daniel>
# -*- mode: python; -*-


## Required modules
#------------------------------------------------------------------------------
from sys import version_info as info


##
## Define static boolean variables based on the version of Python running
#------------------------------------------------------------------------------
class PVersion :

  Two = info.major == 2  # Dead version of Python?  True or False
  Three = info.major == 3  # Up todate version Python?  True of False
